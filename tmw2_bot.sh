# User Settings
USER="login_name";
MANAPLUS="1.9.3.23";

# Server settings
SERVER="server.tmw2.org";

# Logic
while :; do
    xdotool search --sync --name "ManaPlus $MANAPLUS - $USER $SERVER" key "ctrl" ;
    sleep 1 ;
    xdotool search --sync --name "ManaPlus $MANAPLUS - $USER $SERVER" key z z ;
    sleep 1 ;
done

